/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufpr.tads.model;

import java.io.InputStream;

/**
 *
 * @author Walter
 */
public class Pokemons {
    private int id;
    private InputStream foto1;
    private InputStream foto2;
    private String nome;
    private String especie;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public InputStream getFoto1() {
        return foto1;
    }

    public void setFoto1(InputStream foto1) {
        this.foto1 = foto1;
    }

    public InputStream getFoto2() {
        return foto2;
    }

    public void setFoto2(InputStream foto2) {
        this.foto2 = foto2;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }
    
    
}
