/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufpr.tads.dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Walter
 */
public class ConnectionFactory {
    private static final String url = "jdbc:mysql://localhost/pokeserver"; 
    private static final String driverName = "com.mysql.jdbc.Driver";   
    private static final String username = "root";
    private static final String password = "1234"; 
    private static Connection conn;
    
   public static Connection getConnection() {
        try {
            Class.forName(driverName);
            try {
                conn = DriverManager.getConnection(url, username, password);
                System.out.println("Conectado!");
            } catch (SQLException ex) {
                // log an exception. fro example:
                System.out.println("Failed to create the database connection."); 
            }
        } catch (ClassNotFoundException ex) {
            // log an exception. for example:
            System.out.println("Driver not found."); 
        }
        return conn;
    }
}
