/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufpr.tads.dao;

import br.ufpr.tads.model.Pokemons;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.rowset.serial.SerialBlob;
import org.apache.tomcat.util.codec.binary.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Walter
 */
public class PokemonsDAO {
    
    private final String stmtConsultar = "SELECT * FROM pokemons";
    private final String stmtBuscarTreinador = "SELECT nome FROM treinadores WHERE id = ?";
    private final String stmtPesquisar = "SELECT * FROM pokemons WHERE nome = ? ";
    private final String stmtBuscarFotoPrincipal = "SELECT foto1 FROM pokemons WHERE id = ? ";
    private final String stmtIdPokemon = "SELECT id FROM pokemons WHERE nome = ?";
    private final String stmtInserirPokemon = "INSERT INTO pokemons(foto1,foto2,nome,especie,peso,altura,id_treinador) "
            + "VALUES(?,?,?,?,?,?,?)";
    
    public JSONArray consultarPokemons() throws IOException{
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Map<String,Pokemons>pokemons = new HashMap<>();
        
        JSONArray pokeArray = new JSONArray();
        try{
            connection = new ConnectionFactory().getConnection();
            stmt = connection.prepareStatement(stmtConsultar);
            rs = stmt.executeQuery();
            while(rs.next()){
                JSONObject json = new JSONObject();
                json.put("id",rs.getInt("id"));
                json.put("nome",rs.getString("nome"));
                
                InputStream is = rs.getBinaryStream("foto1");
                byte[] imageBytes = rs.getBytes("foto1");
                is.read(imageBytes,0,imageBytes.length);
                is.close();
                String imageStr = Base64.encodeBase64String(imageBytes);
                
                json.put("foto1", imageStr);
                
                InputStream is2 = rs.getBinaryStream("foto2");
                byte[] imageBytes2 = rs.getBytes("foto2");
                is2.read(imageBytes2,0,imageBytes2.length);
                is2.close();
                String imageStr2 = Base64.encodeBase64String(imageBytes2);
                
                json.put("foto2", imageStr2);
                json.put("especie", rs.getString("especie"));
                json.put("peso",rs.getString("peso"));
                json.put("altura",rs.getString("altura"));
                json.put("treinador",buscarTreinador(rs.getInt("id_treinador")));
                pokeArray.add(json);
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){ 
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
        return pokeArray;
    }
    
    public String buscarTreinador(Integer id){
    
    Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String nome ="";
        try{
            connection = new ConnectionFactory().getConnection();
            stmt = connection.prepareStatement(stmtBuscarTreinador);
            stmt.setInt(1, id);
            rs = stmt.executeQuery();
            if(rs.next()){
                nome = rs.getString("nome");
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){ 
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
        return nome;   
    }
    
     public JSONArray pesquisarPokemons() throws IOException{
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        JSONArray pokeArray = new JSONArray();
        try{
            connection = new ConnectionFactory().getConnection();
            stmt = connection.prepareStatement(stmtPesquisar);
            rs = stmt.executeQuery();
            if(rs.next()){
                JSONObject json = new JSONObject();
                json.put("id",rs.getInt("id"));
                json.put("nome",rs.getString("nome"));
                
                InputStream is = rs.getBinaryStream("foto1");
                byte[] imageBytes = rs.getBytes("foto1");
                is.read(imageBytes,0,imageBytes.length);
                is.close();
                String imageStr = Base64.encodeBase64String(imageBytes);
                
                json.put("foto1", imageStr);
                
                InputStream is2 = rs.getBinaryStream("foto2");
                byte[] imageBytes2 = rs.getBytes("foto2");
                is2.read(imageBytes2,0,imageBytes2.length);
                is2.close();
                String imageStr2 = Base64.encodeBase64String(imageBytes2);
                
                json.put("foto2", imageStr2);
                json.put("especie", rs.getString("especie"));
                json.put("peso",rs.getString("peso"));
                json.put("altura",rs.getString("altura"));
                json.put("treinador",buscarTreinador(rs.getInt("id_treinador")));
                pokeArray.add(json);
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){ 
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
        return pokeArray;
    }
     public String carregarFotoPrincipal(Integer idPokemon) throws IOException{
         
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String imageStr = "";
        try{
            connection = new ConnectionFactory().getConnection();
            stmt = connection.prepareStatement(stmtBuscarFotoPrincipal);
            stmt.setInt(1, idPokemon);
            rs = stmt.executeQuery();
            if(rs.next()){
                InputStream is = rs.getBinaryStream("foto1");
                byte[] imageBytes = rs.getBytes("foto1");
                is.read(imageBytes,0,imageBytes.length);
                is.close();
                imageStr = Base64.encodeBase64String(imageBytes);
                
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){ 
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
        return imageStr;
     
     }

    public Integer buscarIdPorNome(String nome) {
        
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Integer id = -1;
        try{
            connection = new ConnectionFactory().getConnection();
            stmt = connection.prepareStatement(stmtIdPokemon);
            stmt.setString(1, nome);
            rs = stmt.executeQuery();
            if(rs.next()){                
               id =Integer.parseInt(rs.getString("id"));
            } 
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){ 
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
        return id;
        
    }

    public Integer inserir(String nome, String especie, String peso, String altura, String imagemStr, Integer usuarioId) {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Integer id = -1;
        try{
            connection = new ConnectionFactory().getConnection();
            stmt = connection.prepareStatement(stmtInserirPokemon);
            byte[] decodedByte = Base64.decodeBase64(imagemStr);
            Blob b = new SerialBlob(decodedByte);
            stmt.setBlob(1,b);
            stmt.setBlob(2, b);
            stmt.setString(3,nome);
            stmt.setString(4,especie);
            stmt.setFloat(5, Float.parseFloat(peso));
            stmt.setFloat(6, Float.parseFloat(altura));
            stmt.setInt(7,usuarioId);
            
            stmt.executeUpdate();
            stmt.close();
            id = buscarIdPorNome(nome);
            
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
        return id;
    }
     
}
