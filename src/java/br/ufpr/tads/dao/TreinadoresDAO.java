/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufpr.tads.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 *
 * @author Walter
 */
public class TreinadoresDAO {
    
    private final String stmtAutenticar = "SELECT id, nome, login, senha FROM treinadores WHERE login = ? and senha = ?";
    private final String stmtIdTreinador = "SELECT id FROM treinadores WHERE nome = ?";
    private final String stmtAtualizarFavorito = "UPDATE treinadores SET favorito = ? WHERE "
            + "id = ?";
    private final String stmtFavorito = "SELECT favorito FROM treinadores WHERE id = ?";
    
    public String validarTreinador(String login, String senha){
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String user ="invalid";
        try{
            connection = new ConnectionFactory().getConnection();
            stmt = connection.prepareStatement(stmtAutenticar);
            stmt.setString(1, login);
            stmt.setString(2, senha);
            rs = stmt.executeQuery();
            if(rs.next()){                
               user = rs.getString("nome");
            } 
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){ 
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
        return user;
    }        
    
    public Integer buscarIdPorNome(String nome){
    
    Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Integer id = -1;
        try{
            connection = new ConnectionFactory().getConnection();
            stmt = connection.prepareStatement(stmtIdTreinador);
            stmt.setString(1, nome);
            rs = stmt.executeQuery();
            if(rs.next()){                
               id =Integer.parseInt(rs.getString("id"));
            } 
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){ 
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
        return id;
    
    }

    public boolean atualizarPokemonFavorito(Integer idTreinador, Integer idPokemon, Integer fav) {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        boolean atualizou = false;
        try{
            connection = new ConnectionFactory().getConnection();
            stmt = connection.prepareStatement(stmtFavorito);
            stmt.setInt(1, idTreinador);
            Integer pokemonFavBD = -1;
            rs = stmt.executeQuery();
            if(rs.next()){                
               pokemonFavBD = rs.getInt("favorito");
            }
            if(pokemonFavBD.equals(idPokemon) && fav == 0){
                //se o pokemon ja for favorito e o cara setou para desmarcar
                stmt = connection.prepareStatement(stmtAtualizarFavorito);
                stmt.setNull(1, Types.INTEGER);
                stmt.setInt(2,idTreinador);
                stmt.executeUpdate();
                stmt.close();
                atualizou = true;
            }else if(!pokemonFavBD.equals(idPokemon) && fav == 1){
                //se for outro pokemon favorito no bd e ele marcou este como fav
                stmt = connection.prepareStatement(stmtAtualizarFavorito);
                stmt.setInt(1, idPokemon);
                stmt.setInt(2,idTreinador);
                stmt.executeUpdate();
                stmt.close();
                atualizou = true;
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){ 
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
        return atualizou;
    }
    
    public Integer buscarFavorito(Integer idTreinador){
        
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Integer idFavorito = -1;
        try{
            connection = new ConnectionFactory().getConnection();
            stmt = connection.prepareStatement(stmtFavorito);
            stmt.setInt(1, idTreinador);
            rs = stmt.executeQuery();
            if(rs.next()){
                idFavorito = rs.getInt("favorito");
            }
        } catch (SQLException ex) {
            throw new RuntimeException("Erro ao consultar contato no banco de dados. Origem="+ex.getMessage());
        } finally{
            try{
                rs.close();
            } catch (SQLException ex){ 
                System.out.println("Erro ao fechar result set. Ex="+ex.getMessage());
            }
            try{
                stmt.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar stmt. Ex="+ex.getMessage());
            }
            try{
                connection.close();
            } catch (SQLException ex){
                System.out.println("Erro ao fechar conexão. Ex="+ex.getMessage());
            }
        }
        return idFavorito;
    }
}
