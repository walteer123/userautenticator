/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufpr.tads.controller;

import br.ufpr.tads.dao.PokemonsDAO;
import br.ufpr.tads.dao.TreinadoresDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;

/**
 *
 * @author Walter
 */
@WebServlet(name = "PokemonsOp", urlPatterns = {"/PokemonsOp"})
public class PokemonsOp extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        TreinadoresDAO treinadoresDAO = new TreinadoresDAO();
        PokemonsDAO pokemonsDAO = new PokemonsDAO();
        PrintWriter out = response.getWriter();
        switch (action) {
            case "updateFav":
                treinadoresDAO = new TreinadoresDAO();
                String nomeTreinador = request.getParameter("nameTr");
                Integer fav = Integer.parseInt(request.getParameter("fav"));
                Integer idPokemon = Integer.parseInt(request.getParameter("idPok"));
                Integer idTreinador = treinadoresDAO.buscarIdPorNome(nomeTreinador);

                boolean atualizou = treinadoresDAO.atualizarPokemonFavorito(idTreinador, idPokemon, fav);
                out = response.getWriter();
                JSONObject json = new JSONObject();
                json.put("atualizou", atualizou);
                response.setContentType("application/json");
                out.print(json);
                out.flush();
                break;
            case "checkFav":
                treinadoresDAO = new TreinadoresDAO();
                Integer id = treinadoresDAO.buscarIdPorNome(request.getParameter("nameTr"));
                Integer idFav = treinadoresDAO.buscarFavorito(id);
                json = new JSONObject();
                json.put("fav", String.valueOf(idFav));
                response.setContentType("application/json");
                out = response.getWriter();
                out.print(json);
                out.flush();
                break;
            case "loadMainImg":
                pokemonsDAO = new PokemonsDAO();
                treinadoresDAO = new TreinadoresDAO();
                JSONObject jsonFoto = new JSONObject();
                id = treinadoresDAO.buscarIdPorNome(request.getParameter("nameTr"));
                idFav = treinadoresDAO.buscarFavorito(id);
                if (idFav != 0) {
                    jsonFoto.put("foto", pokemonsDAO.carregarFotoPrincipal(idFav));
                } else {
                    jsonFoto.put("foto", "");
                }
                response.setContentType("application/json");
                out = response.getWriter();
                out.print(jsonFoto);
                out.flush();
                break;
            case "insertPokemon":
                pokemonsDAO = new PokemonsDAO();
                treinadoresDAO = new TreinadoresDAO();
                String nome = request.getParameter("nome");
                Integer idP = pokemonsDAO.buscarIdPorNome(nome);
                json = new JSONObject();
                if (idP > 0) {
                    //ja tenho um pokemon com esse nome cadastrado !
                    json.put("insert", "idExists");

                } else {
                    String especie = request.getParameter("especie");
                    String peso = request.getParameter("peso");
                    String altura = request.getParameter("altura");
                    String ehFavorito = request.getParameter("favorito");
                    String imagemStr = request.getParameter("imagem");
                    String usuarioLogado = request.getParameter("userCadastro");
                    Integer idT = treinadoresDAO.buscarIdPorNome(usuarioLogado);
                    Integer idCadastrado = pokemonsDAO.inserir(nome, especie, peso, altura, imagemStr, idT);
                    if ("1".equals(ehFavorito) && idCadastrado>0) {
                        treinadoresDAO.atualizarPokemonFavorito(idT, idCadastrado, Integer.parseInt(ehFavorito));
                    }
                    if(idCadastrado>0)
                        json.put("insert","OK");
                    else
                        json.put("insert","Error");
                }
                response.setContentType("application/json");
                out.print(json);
                out.flush();
                break;

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
